import 'reflect-metadata';
import { Application as App } from '../main';

const app = new App();

app.start()
    .catch(err => {
        app.logger.error('Failed to start', err);
        process.exit(1);
}); 

import { injectable } from 'inversify';
import mongoose from 'mongoose';
import { FrameworkEnv } from './env';
import { Logger } from './logger';

@injectable()
export class MongoDb {
    constructor(
        private frameworkEnv: FrameworkEnv,
        private logger: Logger,
    ) {}

    async connect() {
        const database = mongoose.connection;
        
        await mongoose.connect(this.frameworkEnv.MONGODB_CONNECTION_URL, { useNewUrlParser: true });

        database.on('open', () => this.logger.info('Successfuly connected to database.'));
        database.on('error', () => this.logger.error('Failed to connect to database.'));
    }

    async close() {
        await mongoose.connection.close();
    }
}

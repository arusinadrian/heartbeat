import { Container } from 'inversify';
import { Logger, StandardLogger } from './logger';
import { HttpServer } from './http';
import { MetricsRouter } from './metrics/route';
import { Router } from './router';
import { FrameworkEnv } from './env';
import { MongoDb } from './mongodb';
import { ApplicationRepo } from './modules/heartbeat/repos/applicationRepo';
import { JobQueue } from './modules/heartbeat/job-queue/job-queue';
import { ApplicationStateService } from './modules/heartbeat/services/applicationStateService';
import { HeartbeatService } from './modules/heartbeat/services/heartbeatService';
import { MainRouter } from './modules/heartbeat/rest/routes';
import { controllers } from './modules/heartbeat/rest/controller.module';
import { MetricsRegistry, getGlobalMetrics } from './metrics';

/**
 * Application provides an IoC container where all modules should be registered
 * and provides minimal lifecycle framework (start, stop, beforeStart, afterStop).
 *
 * Note: it is convenient (less typing, fewer possibilities for human errors)
 * for a typical http server application to bind its lifecycle to http server lifecycle,
 * so currently Application combines concerns of both http server and IoC composition root.
 * If this proves problematic in future, we may choose to change that and decouple the two.
 *
 * Despite depending on Koa, Application can run just fine without starting an http server.
 * Simply avoid invoking `app.startServer()` and manage the app lifecycle separately
 * (e.g invoke `app.runStartHooks()` instead of `app.startServer()`).
 */
export class Application {
    container: Container;

    constructor() {
        const container = new Container({ skipBaseClassChecks: true });
        this.container = container;
        this.container.bind('RootContainer').toConstantValue(container);
        this.container.bind(HttpServer).toSelf().inSingletonScope();
        this.container.bind(FrameworkEnv).toSelf().inSingletonScope();
        this.container.bind(Logger).to(StandardLogger).inSingletonScope();
        this.container.bind(Router).to(MetricsRouter);
        this.container.bind(MongoDb).toSelf().inSingletonScope();
        this.container.bind(ApplicationRepo).toSelf();

        this.container.load(controllers);
        this.bindRouter(MainRouter);

        this.container.bind(ApplicationStateService).toSelf();
        this.container.bind(HeartbeatService).toSelf();
        this.container.bind(JobQueue).toSelf().inSingletonScope();
        this.container.bind(MetricsRegistry).toConstantValue(getGlobalMetrics());
    }

    get logger(): Logger {
        return this.container.get(Logger);
    }

    get httpServer(): HttpServer {
        return this.container.get(HttpServer);
    }

    async beforeStart(): Promise<void> {
        await this.mongoDb.connect();
        await this.httpServer.startServer();
        await this.jobQueue.startJobQueues();
    }

    async afterStop(): Promise<void> {
        this.mongoDb.close();
        await this.jobQueue.stopJobQueues();
        await this.httpServer.stopServer();
    }

    bindRouter(constructor: (new(...args: any[]) => Router)): this {
        this.container.bind(Router).to(constructor);
        return this;
    }

    async start() {
        process.on('SIGTERM', () => this.stop());
        process.on('SIGINT', () => this.stop());
        await this.beforeStart();
    }

    async stop() {
        // TODO uninstall process signals handlers better
        process.removeAllListeners();
        await this.afterStop();
    }

    get mongoDb() {
        return this.container.get<MongoDb>(MongoDb);
    }

    get jobQueue() {
        return this.container.get<JobQueue>(JobQueue);
    }
}

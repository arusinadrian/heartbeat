import { injectable } from "inversify";
import { ApplicationRepo } from "../repos/applicationRepo";
import { HeartbeatDTO } from "../rest/dtos/HeartbeatDTO";
import { Application } from "../entities/application";
import { toPersistance, toDomain } from "../mappers/applicationMapp";

@injectable()
export class HeartbeatService {
    constructor(
        protected applicationRepo: ApplicationRepo,
    ) {}

    public async ping(request: HeartbeatDTO) {
        const applicationId = request.id;

        const applicationAlreadyExists = await this.applicationRepo.findById(applicationId);

        if (applicationAlreadyExists) {
            const existingApplication = toDomain(applicationAlreadyExists);
            existingApplication.updatedAt = Date.now();
            await this.applicationRepo.update(applicationId, toPersistance(existingApplication));
            return existingApplication;
        }

        const application = Application.fromJSON(request);
        const appModel = toPersistance(application);

        await this.applicationRepo.create(appModel);

        return application;
    }
}


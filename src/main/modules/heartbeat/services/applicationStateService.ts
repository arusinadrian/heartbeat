import { injectable } from "inversify";
import { ApplicationRepo } from "../repos/applicationRepo";
import { toDomain } from "../mappers/applicationMapp";
import { FrameworkEnv } from "../../../env";
import { IApplicationModel } from "../infra/database/applicationModel";
import { Logger } from "../../../logger";

@injectable()
export class ApplicationStateService {
    constructor ( 
        protected applicationRepo: ApplicationRepo,
        protected frameworkEnv: FrameworkEnv,
        protected logger: Logger,
    ) {}

    public async getAllApplications() {
        const fetchedApps = await this.applicationRepo.find({});

        const result = fetchedApps.map(app => toDomain(app));

        return result;
    }

    public async removeApplication(id: string) {
        const exists = await this.applicationRepo.findById(id);
        
        if (!exists) return false;

        await this.applicationRepo.delete(id);

        return true;
    }

    public async getAllApplicationsByGroupName(group: string) {
        const fetchedAppsByGroup = await this.applicationRepo.find({ group });

        const result = fetchedAppsByGroup.map(app => toDomain(app));

        return result;
    }

    public async removeExpiredApplications() {
        const fetchedApps: Array<IApplicationModel> = await this.applicationRepo.find({ 
            updatedAt: { $lt: this.frameworkEnv.EXPIRE_OLDER_THAN } 
        });

        this.logger.info(`Removing ${fetchedApps.length} expired applications.`);

        await Promise.all(
            fetchedApps.map(async app => await this.applicationRepo.delete(app._id))
        );

        return;
    }   
}

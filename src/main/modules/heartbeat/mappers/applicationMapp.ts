import { Application } from "../entities/application";
import { IApplicationModel } from "../infra/database/applicationModel";

export function toDomain(raw: any): Application {
    return Application.fromJSON({
        id: raw._id,
        group: raw.group,
        createdAt: raw.createdAt,
        updatedAt: raw.updatedAt,
        meta: raw?.meta,
    });
}

export function toPersistance(application: Application): IApplicationModel {
    const createdAt = application?.createdAt | Date.now();
    const updatedAt = application?.updatedAt | Date.now();
    
    const app = ({
        _id: application.id,
        group: application.group,
        createdAt,
        updatedAt,
        meta: application?.meta,
    } as IApplicationModel);

    return app;
}

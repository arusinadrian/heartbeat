import * as mongoose from 'mongoose';

interface Read<T> {
    findById: (id: string) => Promise<Object | null>; 
    find(cond: Object, fields: Object, options: Object): Promise<mongoose.Query<any[]>>;
} 

interface Write<T> {
    create: (item:T ) => Promise<void>;
    update:(_id: string, item:T) => Promise<void> ;
    delete: (_id: string) => Promise<void | Error>;
}
export class RepositoryBase<T extends mongoose.Document> implements Read<T>, Write<T> {
    private _model: mongoose.Model<mongoose.Document>;

    constructor(schemaModel: mongoose.Model<mongoose.Document>) {
        this._model = schemaModel;
    }

    async create(item: T) {
        await this._model.create(item);
    }

    async update(_id: string, item: T) {
        this._model.update({ _id }, item);
    }

    async delete(_id: string) {
        await this._model.deleteOne({ _id });
    }

    async findById(_id: string) {
            return this._model.findById(_id);
    }

    async find(cond?: any, fields?: Object, options?: Object): Promise<mongoose.Query<any[]>> {
        return this._model.find(cond, options);
    }
}

import * as mongoose from 'mongoose';

export interface IApplicationModel extends mongoose.Document {
    _id: string;
    group: string;
    createdAt: number;
    updatedAt: number;
    meta: object | null;
}

export class ApplicationModel {
    private _applicationModel: IApplicationModel;
    
    constructor(applicationModel: IApplicationModel) {
        this._applicationModel = applicationModel;
    }

    get meta() {
        return this._applicationModel.meta;
    }
    
    get _id() {
        return this._applicationModel.id;
    }
    
    get group() {
        return this._applicationModel.group;
    }
    
    get updatedAt() {
        return this._applicationModel.updatedAt;
    }
}


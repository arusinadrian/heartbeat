import * as mongoose from 'mongoose';

const AppplicationSchema = new mongoose.Schema({
    _id : {
        type: String,
        required: true
    },
    group: {
        type: String,
        required: true
    },
    createdAt: {
        type: Number,
        required: true
   },
   updatedAt: {
        type: Number,
        required: true
   },
   meta: {
       type: Object,
       required: false,
   }
});

export const application =  mongoose.model('Applications', AppplicationSchema, 'applications');

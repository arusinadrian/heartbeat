import { injectable } from 'inversify';
import { RepositoryBase } from  '../infra/database/repositoryBase';
import { IApplicationModel } from '../infra/database/applicationModel';
import { application }  from '../infra/database/applicationSchema';

@injectable()
export class ApplicationRepo extends RepositoryBase<IApplicationModel> {
    constructor() {
        super(application);
    }
}

import { Entity, Field } from '../../../entity';

export class Application extends Entity {
    @Field({
        schema: { type: 'string', minLength: 24, maxLength: 24 }
    })
    id: string = '';

    @Field({ schema: { type: 'string' } })
    group: string = '';

    @Field({ nullable: true, schema: { type: 'object' } })
    meta: object | null = null;

    @Field({ schema: { type: 'number' } })
    createdAt: number = Date.now();
    
    @Field({ schema: { type: 'number' } })
    updatedAt: number = Date.now();
}

import { injectable } from "inversify";
import { BaseController } from "./BaseController";
import { ApplicationStateService } from "../services/applicationStateService";
import { RemoveApplicationDTO } from "./dtos/RemoveApplicationDTO";
import { Response } from "koa";

@injectable() 
export class RemoveApplicationController extends BaseController {
        constructor(
            private applicationStateService: ApplicationStateService,
        ) {
            super();
        }

        async executeImpl(req: RemoveApplicationDTO, res: Response) {
            const result = await this.applicationStateService.removeApplication(req.id);

            if (result) {
                res.status = 204;
                return res;
            } 

            res.status = 410;
            res.message = 'Could not delete not existing application heartbeat.';
            return res;
        }
}

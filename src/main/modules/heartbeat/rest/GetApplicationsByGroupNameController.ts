import { injectable } from "inversify";
import { BaseController } from "./BaseController";
import { ApplicationStateService } from "../services/applicationStateService";
import { GetApplicationsByGroupNameDTO } from "./dtos/GetApplicationsByGroupNameDTO";

@injectable() 
export class GetApplicationsByGroupNameController extends BaseController {
        constructor(
            private applicationStateService: ApplicationStateService,
        ) {
            super();
        }

        async executeImpl(req: GetApplicationsByGroupNameDTO) {
            return this.applicationStateService.getAllApplicationsByGroupName(req.group);
        }
}

import { Request, Response } from 'koa';

export abstract class BaseController {
    protected abstract executeImpl (req: Request | any, res: Response): Promise <void | any>;
}

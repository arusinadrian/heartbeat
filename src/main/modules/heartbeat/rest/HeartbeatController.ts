import { injectable } from "inversify";
import { BaseController } from "./BaseController";
import { HeartbeatService } from "../services/heartbeatService";
import { HeartbeatDTO } from "./dtos/HeartbeatDTO";

@injectable() 
export class HearbeatController extends BaseController {
        constructor(
            private heartbeatService: HeartbeatService,
        ) {
            super();
        }

        async executeImpl(req: HeartbeatDTO) {
            return this.heartbeatService.ping(req);
        }
}


import { injectable } from "inversify";
import { BaseController } from "./BaseController";
import { ApplicationStateService } from "../services/applicationStateService";

@injectable() 
export class GetAllApplicationsController extends BaseController {
        constructor(
            private applicationStateService: ApplicationStateService,
        ) {
            super();
        }

        async executeImpl() {
            return this.applicationStateService.getAllApplications();
        }
}

import { ContainerModule, interfaces } from "inversify";
import { GetAllApplicationsController } from "./GetAllApplicationController";
import { GetApplicationsByGroupNameController } from "./GetApplicationsByGroupNameController";
import { RemoveApplicationController } from "./RemoveApplicationController";
import { HearbeatController } from "./HeartbeatController";

export const controllers = new ContainerModule((bind: interfaces.Bind) => {
    bind(GetAllApplicationsController).toSelf();
    bind(GetApplicationsByGroupNameController).toSelf();
    bind(RemoveApplicationController).toSelf();
    bind(HearbeatController).toSelf();
});



import { Router, Get, Post } from '../../..';
import { injectable } from 'inversify';
import { Delete, PathParam } from '../../../router';
import { RemoveApplicationDTO } from './dtos/RemoveApplicationDTO';
import { GetAllApplicationsController } from './GetAllApplicationController';
import { GetApplicationsByGroupNameController } from './GetApplicationsByGroupNameController';
import { GetApplicationsByGroupNameDTO } from './dtos/GetApplicationsByGroupNameDTO';
import { RemoveApplicationController } from './RemoveApplicationController';
import { HeartbeatDTO } from './dtos/HeartbeatDTO';
import { HearbeatController } from './HeartbeatController';

@injectable()
export class MainRouter extends Router {
    constructor(
        protected getAllApplicationsController: GetAllApplicationsController,
        protected getApplicationsByGroupNameController: GetApplicationsByGroupNameController,
        protected removeApplicationController: RemoveApplicationController,
        protected heartbeatController: HearbeatController,
    ) {
        super();
    }

    @Post({ 
        path: '/{group}/{id}',
        requestBodySchema: {
            type: 'object',
            properties: {
                meta: { type: 'object' }
            },
        },
        responses: {
            200: { schema: { type: 'object' } },
        }
    })
    async create(
        @PathParam('group', { schema: { type: 'string'} }) group: string,
        @PathParam('id', { schema: { type: 'string'} }) id: string,
        
    ) {
        const meta = this.ctx.request.body;
        const heartbeatDTO: HeartbeatDTO = {
            group,
            id,
            meta,
        };

        return this.heartbeatController.executeImpl(heartbeatDTO);
    }

    @Get({
        path: '/',
        responses: {
            200: { schema: { type: 'array', items: { type: 'object' } } }
        }
    })
    async getAll() {
        return this.getAllApplicationsController.executeImpl();
    }

    @Get({
        path: '/{group}',
        responses: {
            200: { schema: { type: 'array', items: { type: 'object' } } }
        }
    })
    async getByGroupName(
        @PathParam('group', { schema: { type: 'string'} }) group: string,
    ) {
        const req: GetApplicationsByGroupNameDTO = { group };
        return this.getApplicationsByGroupNameController.executeImpl(req);
    }


    @Delete({ 
        path: '/{group}/{id}',
        responses: {
            204: {},
            410: { schema: { type: 'object' } },
        }
    })
    async remove(
        @PathParam('group', { schema: { type: 'string'} }) group: string,
        @PathParam('id', { schema: { type: 'string'} }) id: string,
    ) {
        const req: RemoveApplicationDTO = {
            group,
            id,
        };
        
        return this.removeApplicationController.executeImpl(req, this.ctx.response);
    }
}

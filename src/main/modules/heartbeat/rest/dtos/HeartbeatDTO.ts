export interface HeartbeatDTO {
    group: string;
    id: string;
    meta?: object;
}

export interface RemoveApplicationDTO {
    group: string,
    id: string,
}

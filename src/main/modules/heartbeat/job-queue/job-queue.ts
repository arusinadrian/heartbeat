import { injectable } from "inversify";
import Agenda from 'agenda';
import { ApplicationStateService } from "../services/applicationStateService";
import { FrameworkEnv } from "../../../env";


const REMOVE_EXPIRING_APPLICATIONS_JOB_NAME = 'RemoveExpiringApplications';

@injectable()
export class JobQueue {
    private agenda: Agenda;
    constructor(
        private applicationStateService: ApplicationStateService,
        private frameworkEnv: FrameworkEnv,
    ) {
        const connectionOpts = {
            db: {
                address: `${this.frameworkEnv.MONGODB_CONNECTION_URL}`,
                collection: 'agendaJobs',
            },
        };
        
        this.agenda = new Agenda(connectionOpts);
    }

    private registerJobs() {
        this.agenda.define(REMOVE_EXPIRING_APPLICATIONS_JOB_NAME, async () => {
            await this.applicationStateService.removeExpiredApplications();
        });

        this.agenda.every(this.frameworkEnv.REMOVE_EXPIRED_APPLICATIONS_INTERVAL, REMOVE_EXPIRING_APPLICATIONS_JOB_NAME);
    }


    async startJobQueues() {
        await this.agenda.start();    
        this.registerJobs();
    }

    async stopJobQueues() {
        await this.agenda.stop();
    }
}

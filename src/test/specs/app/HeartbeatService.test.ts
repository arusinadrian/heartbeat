import { Container } from "inversify";
import { ApplicationRepo } from "../../../main/modules/heartbeat/repos/applicationRepo";
import assert from "assert";
import { HeartbeatService } from "../../../main/modules/heartbeat/services/heartbeatService";
import { ApplicationRepoInMemory } from "../infrastructure/applicationRepoInMemory";
import { HeartbeatDTO } from "../../../main/modules/heartbeat/rest/dtos/HeartbeatDTO";

export function timeout(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

describe('HeartbeatService', () => {
    let container: Container;
    let heartbeatService: HeartbeatService;
    let applicationRepo: ApplicationRepoInMemory;

    const newApp: HeartbeatDTO = {
        group: 'particle-detector',
        id: 'e335175a-eace-4a74-b99c-c6466b6afadd',
        meta: {
            foo: 1,
        }
    };

    beforeEach(() => {
        container = new Container({ skipBaseClassChecks: true });
        container.bind(ApplicationRepo).to(ApplicationRepoInMemory).inSingletonScope();
        container.bind(HeartbeatService).toSelf();

        heartbeatService = container.get(HeartbeatService);
        applicationRepo = (container.get(ApplicationRepo) as ApplicationRepoInMemory);
    });

    it('should add application heartbeat', async () => {
        await heartbeatService.ping(newApp);

        const heatbeats = await applicationRepo.find({});

        assert.equal(heatbeats.length, 1);
    });


    it('should update updatedAt time if application heartbeat already exists', async () => {
        const oldHeartbeat = await heartbeatService.ping(newApp);
        await timeout(100);
        const newHeartbeat = await heartbeatService.ping(newApp);

        const heatbeats = await applicationRepo.find({});
        assert.equal(newHeartbeat.updatedAt > oldHeartbeat.updatedAt, true);
        assert.equal(heatbeats.length, 1);
    });
});

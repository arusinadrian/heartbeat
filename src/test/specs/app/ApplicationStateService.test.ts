import { Container } from "inversify";
import { ApplicationRepo } from "../../../main/modules/heartbeat/repos/applicationRepo";
import assert from "assert";
import { ApplicationRepoInMemory } from "../infrastructure/applicationRepoInMemory";
import { ApplicationStateService } from "../../../main/modules/heartbeat/services/applicationStateService";
import { IApplicationModel } from "../../../main/modules/heartbeat/infra/database/applicationModel";
import { Logger, StandardLogger } from "../../../main";
import { FrameworkEnv } from "../../../main/env";

describe('HeartbeatService', () => {
    let container: Container;
    let applicationStateService: ApplicationStateService;
    let applicationRepo: ApplicationRepoInMemory;

    const newApp = ({
        group: 'particle-detector',
        _id: 'e335175a-eace-4a74-b99c-c6466b6afadd',
        createdAt: 1597745128024,
        updatedAt: 1597745128024,
        meta: {
            foo: 1,
        }
    } as IApplicationModel);

    const newApp_2 = ({
        group: 'particle-detector',
        _id: 'e335175a-eace-4a74-b99c-c6466b6afade',
        createdAt: 1597745128027,
        updatedAt: 1597745128024,
        meta: {
            foo: 1,
        }
    } as IApplicationModel);

    beforeEach(() => {
        container = new Container({ skipBaseClassChecks: true });
        container.bind(ApplicationRepo).to(ApplicationRepoInMemory).inSingletonScope();
        container.bind(ApplicationStateService).toSelf();
        container.bind(FrameworkEnv).toSelf().inSingletonScope();
        container.bind(Logger).to(StandardLogger).inSingletonScope();

        applicationStateService = container.get(ApplicationStateService);
        applicationRepo = (container.get(ApplicationRepo) as ApplicationRepoInMemory);
    });

    describe('getAllApplications()', () => {
        it('should return empty array if no applications were added', async () => {
            const result = await applicationStateService.getAllApplications();
            
            assert.equal(Array.isArray(result), true);
            assert.equal(result.length, 0);
        });
    
        it('should return array of applications that are being watched if there are some', async () => {
            await applicationRepo.create(newApp);
            await applicationRepo.create(newApp_2);
    
            const result = await applicationStateService.getAllApplications();
    
            assert.equal(result.length, 2);
            assert.equal(result[0].id === newApp._id, true);
            assert.equal(result[1].id === newApp_2._id, true);
        });
    });

    describe('removeApplication()', () => {
        it('should return false if it has no application to remove', async () => {
            const result =  await applicationStateService.removeApplication('123');

            assert.equal(result, false);
        });

        it('should remove application if it does exist', async () => {
            await applicationRepo.create(newApp);

            await applicationStateService.removeApplication(newApp._id);
    
            const appsAfterRemove = await applicationRepo.find({});
            assert.equal(appsAfterRemove.length, 0);
        });
    });

    describe('getAllApplicationByGroupName()', () => {
        const newApp_3 = ({
            group: 'particle-detector_1',
            _id: 'e335175a-eace-4a74-b99c-c6466b6afadf',
            createdAt: 1597745128027,
            updatedAt: 1597745128024,
            meta: {
                foo: 1,
            }
        } as IApplicationModel);

        it('should return only applications with given group name', async () => {
            await applicationRepo.create(newApp);
            await applicationRepo.create(newApp_2);
            await applicationRepo.create(newApp_3);
            const group = newApp.group;

            const result = await applicationStateService.getAllApplicationsByGroupName(group);
            
            assert.equal(result.length, 2);
            assert.equal(result[0].group, group);
            assert.equal(result[1].group, group);
        });
    });

    describe('removeExpiredApplications()', () => {
        const newApp_3 = ({
            group: 'particle-detector_1',
            _id: 'e335175a-eace-4a74-b99c-c6466b6afadf',
            createdAt: Date.now(),
            updatedAt: Date.now(),
            meta: {
                foo: 1,
            }
        } as IApplicationModel);

        it('should remove all applications that have updatedTime older than specificed in configuration', async () => {
            await applicationRepo.create(newApp);
            await applicationRepo.create(newApp_2);
            await applicationRepo.create(newApp_3);

            await applicationStateService.removeExpiredApplications();

            const applications = await applicationRepo.find({});

            assert.equal(applications.length, 1);
            assert.equal(applications[0].updatedAt, newApp_3.updatedAt);
        });
    });
});


import { Application } from "../../../main/application";
import supertest from "supertest";
import assert from "assert";
import { MainRouter } from "../../../main/modules/heartbeat/rest/routes";
import { Router } from "../../../main";
import { ApplicationRepo } from "../../../main/modules/heartbeat/repos/applicationRepo";
import { ApplicationRepoInMemory } from "../infrastructure/applicationRepoInMemory";
import { IApplicationModel } from "../../../main/modules/heartbeat/infra/database/applicationModel";

describe('MainRouter', () => {
    class App extends Application {
        constructor() {
            super();
            this.container.unbind(ApplicationRepo);
            this.container.bind(ApplicationRepo).to(ApplicationRepoInMemory).inSingletonScope();
            this.container.bind(Router).to(MainRouter);
        }

        async beforeStart() {
            await this.httpServer.startServer();
        }

        async afterStop() {
            await this.httpServer.stopServer();
        }

        get applicationRepo() {
            return (this.container.get(ApplicationRepo) as ApplicationRepoInMemory);
        }
    }

    const app = new App();
    beforeEach(() => app.start());
    afterEach(() => app.stop());

    it('GET /', async () => {
        const request = supertest(app.httpServer.callback());
        const res = await request.get('/');
        
        assert.equal(res.status, 200);
        assert.equal(Array.isArray(res.body), true);
        assert.equal(res.body.length, 0);
    });
    
    it('GET /{group}', async () => { 
        const request = supertest(app.httpServer.callback());
        const res = await request.get('/123');
        
        assert.equal(res.status, 200);
        assert.equal(Array.isArray(res.body), true);
        assert.equal(res.body.length, 0);
    });
    
    it('POST /{group}/{id}', async () => {
        const request = supertest(app.httpServer.callback());
        const res = await request.post('/particle-detector/e335175a-eace-4a74-b99c-c6466b6afadz')
            .send({ meta: { foo : 1} } );
        
        assert.equal(res.status, 200);
        assert.equal('updatedAt' in res.body, true);
        assert.equal('createdAt' in res.body, true);
    });

    it('DEL /{group}/{id} existing application', async () => { 
        const newApp = ({
            group: 'particle-detector',
            _id: 'e335175a-eace-4a74-b99c-c6466b6afadd',
            createdAt: 1597745128024,
            updatedAt: 1597745128024,
            meta: {
                foo: 1,
            }
        } as IApplicationModel);

        const request = supertest(app.httpServer.callback());

        await app.applicationRepo.create(newApp);

        const res = await request.delete(`/${newApp.group}/${newApp._id}`);

        assert.equal(res.status, 204);
    });

    it('DEL /{group}/{id} not existing application', async () => { 
        const request = supertest(app.httpServer.callback());

        const res = await request.delete('/somegroup/123');

        assert.equal(res.status, 410);
    });
});


import { ApplicationRepoInMemory } from './applicationRepoInMemory';
import { IApplicationModel } from "../../../main/modules/heartbeat/infra/database/applicationModel";
import assert from 'assert';
import { Container } from 'inversify';
import { ApplicationRepo } from '../../../main/modules/heartbeat/repos/applicationRepo';

describe('ApplicationRepoInMemory', () => {
    let container: Container;
    let inMemoryRepo: ApplicationRepoInMemory;

    const newApp = ({
        group: 'particle-detector',
        _id: 'e335175a-eace-4a74-b99c-c6466b6afadd',
        createdAt: 1597745128024,
        updatedAt: 1597745128024,
        meta: {
            foo: 1,
        }
    } as IApplicationModel);

    const newerApp = ({
        group: 'particle-detector',
        _id: 'e335175a-eace-4a74-b99c-c6466b6afade',
        createdAt: 1597745128024,
        updatedAt: 1597745128024,
        meta: {
            foo: 1,
        }
    } as IApplicationModel);


    beforeEach(() => {
        container = new Container({ skipBaseClassChecks: true });
        container.bind(ApplicationRepo).to(ApplicationRepoInMemory).inSingletonScope();

        inMemoryRepo = (container.get(ApplicationRepo) as ApplicationRepoInMemory);
    });

    
    it('should add to internal Map', async () => {
        await inMemoryRepo.create(newApp);
        
        assert.equal(inMemoryRepo.applicationsArray.size, 1);
        assert.equal(inMemoryRepo.applicationsArray.get(newApp._id), newApp);
    });

    it('should delete from internal Map', async () => {
        await inMemoryRepo.create(newApp);
        
        await inMemoryRepo.delete(newApp._id);
        
        assert.equal(inMemoryRepo.applicationsArray.size, 0);
    });

    it('should update item from internal Map', async () => {
        await inMemoryRepo.create(newApp);
        
        const newGroupName = 'changedGroupName';
        const newApp_2 = newApp;
        newApp_2.group = newGroupName;

        await inMemoryRepo.update(newApp._id, newApp_2);

        assert.equal(inMemoryRepo.applicationsArray.get(newApp._id).group, newGroupName);
    });

    it('should return item by id', async () => {
        await inMemoryRepo.create(newApp);

        const result = await inMemoryRepo.findById(newApp._id);

        assert.equal(result, newApp);
    });

    it('should return all items for {} condition' , async () => { 
        await inMemoryRepo.create(newApp);
        await inMemoryRepo.create(newerApp);

        const result = await inMemoryRepo.find({});

        assert.equal(result.length, 2);
    });

    it('should return items by group name for { group: groupName } condition', async () => {
        await inMemoryRepo.create(newApp);
        await inMemoryRepo.create(newerApp);
        const differentGroupApp = ({
            group: 'particle-detector_1',
            _id: 'e335175a-eace-4a74-b99c-c6466b6afadf',
            createdAt: 1597745128024,
            updatedAt: 1597745128024,
            meta: {
                foo: 1,
            }
        } as IApplicationModel);
        await inMemoryRepo.create(differentGroupApp);

        const result = await inMemoryRepo.find({ group: differentGroupApp.group });

        assert.equal(result.length, 1);
        assert.equal(result[0].group, differentGroupApp.group);
    });


    it('should return items for { updatedAt: {  $lt: amount } } condition', async () => {
        await inMemoryRepo.create(newApp);
        await inMemoryRepo.create(newerApp);
        const currentTimestampApp = ({
            group: 'particle-detector_1',
            _id: 'e335175a-eace-4a74-b99c-c6466b6afadf',
            createdAt: Date.now(),
            updatedAt: Date.now(),
            meta: {
                foo: 1,
            }
        } as IApplicationModel);
        await inMemoryRepo.create(currentTimestampApp);

        const result = await inMemoryRepo.find({ updatedAt: { $lt: 300 }});

        assert.equal(result.length, 2);
    });
});


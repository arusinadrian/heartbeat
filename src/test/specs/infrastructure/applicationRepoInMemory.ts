import { injectable } from "inversify";
import { IApplicationModel } from "../../../main/modules/heartbeat/infra/database/applicationModel";
import { RepositoryBase } from "../../../main/modules/heartbeat/infra/database/repositoryBase";
import * as mongoose from 'mongoose';

@injectable()
export class ApplicationRepoInMemory extends RepositoryBase<IApplicationModel> {
    private _applicationsArray: Map<String, any> = new Map();

    async create(item: IApplicationModel) {
        this._applicationsArray.set(item._id ,item);
    }

    async update<T>(_id: string, item: T) {
        this._applicationsArray.set(_id , item);
    }

    async delete(_id: string) {
        this._applicationsArray.delete(_id);
    }

    async findById(_id: string) {
        return this._applicationsArray.get(_id);
    }

    async find(cond?: any): Promise<mongoose.Query<any[]>> {
        const result = ([] as Array<IApplicationModel>);

        for (const [, app] of this.applicationsArray) {
            switch(true) {
                case Object.keys(cond).length === 0: 
                    result.push(app);
                    break;
                case cond?.group === app.group: 
                    result.push(app);
                    break;
                case 'updatedAt' in cond: 
                    if (Date.now() - Number(cond.updatedAt['$lt']) > app.updatedAt) {
                        result.push(app);
                    }
            }
        }

        return result;
    }

    get applicationsArray() {
        return this._applicationsArray;
    }
}


import { IApplicationModel } from "../../../main/modules/heartbeat/infra/database/applicationModel";
import { toDomain, toPersistance } from "../../../main/modules/heartbeat/mappers/applicationMapp";
import { Application } from "../../../main/modules/heartbeat/entities/application";
import assert from 'assert';

describe('applicationMapp', () => {
    describe('toDomain tests', () => {
        const newApp = ({
             _id: 'e335175a-eace-4a74-b99c-c6466b6afadd',
            group: 'particle-detector',
            createdAt: 1597745128024,
            updatedAt: 1597745128024,
            meta: { foo: 1 } 
        } as IApplicationModel);

        const app = Application.fromJSON({
            id: 'e335175a-eace-4a74-b99c-c6466b6afadd',
            group: 'particle-detector',
            createdAt: 1597745128024,
            updatedAt: 1597745128024,
            meta: { foo: 1 } 
        });

        it('should change application model to application entity', () => {
            const application = toDomain(newApp);
            
            assert.equal(application instanceof Application, true);
            assert.equal(application.id, newApp._id);
        });

        it('should change application entity to persistance object', () => {
            const appModel = toPersistance(app);

            assert.equal(app.id, appModel._id);
        })
    });
});

### Top level architecture decisions 

So to begin with, I didn't see in specification any functionality that was based strictly on having multiple entities. 

The solution could be implemented as a group and application entity separately. I chose not to do it that way for simplicity sake, as well as I chose non-relational database MongoDB as a database, and splitting data into different collections, especially super small like the ones we have here would be really bad practice. So I have one entity. 

The tradeoff of my decision was the fact that we cannot have multiple heartbeats with the same id but different groups. 

Another conscious decision was the fact that we cannot update metadata of already created heartbeat till it expired. The only thing that will happen will be updating time till it expires.

Horizontally scaling and worker polling database, it's obvious that with scaling up of our application we would spawn more workers and it could increase the amount of reading queries on our database. In this case it wouldnt be an issue since we are using agenda and it handles concurrency via using MongoDB as a data source.

The logic for application is in modules/heartbeats, my reasoning for it is that application can have multiple modules that are pretty much-bounded contexts, in this case, my bounded context is hearbeat. 



### Things that I would do a bit differently

I chose to use mongoose and generally MongoDB. Problem with Mongoose is that it has poor typings and sometimes I was forced to do type assertions, especially with IApplicationModel. There are ways to override that but it would take a lot of time. Next time I would probably go with other ORM or just pure MongoDB library. 

For testing sake, I created in memory repo with similar API that is in repositoryBase, since here I don't have complex queries on a database that I would like to test I thought its a valid way. In case of more complex applications, I would either use `mongodb-memory-server` (but it actually runs the whole mongodb just that it downloads binary and runs it in memory). Or just use `mockingoose` with `mingo`. 


### "CI green page"

Since I didn't add CI to gitlab I'll just paste here pictures that linting doesn't throw errors and I have a few passing tests. 

Production-wise I would probably add SonarQube and test coverage since mocha doesn't have it out of the box I would add istanbul. 


![tests](tests.png)
![linter](lint.png)

### Offtopic

I saw that you are often using type assertion on global to any.

```
  (global as any)[METRICS_GLOBAL_KEY]

```

A quirky way to solve that and still have types, although from what I can see you have a rule for no-namespaces so I disabled it for this line.

```typescript
declare global {

    namespace NodeJS { // eslint-disable-line @typescript-eslint/no-namespace
        interface Global {
            [METRICS_GLOBAL_KEY]?: any;
        }
    }    
}


....
 global [METRICS_GLOBAL_KEY] = registry;

```


### How to run application

You need MongoDB instance, by default env for mongodb connection is `'mongodb://0.0.0.0:27017/heartbeat` to override it add different one in .env .

```
docker run -p 27017:27017 -d mongo

npm i
npm run compile && node out/bin/serve.js
```




